### Команда для запуска скрипта:

> gnuplot errorbar.p

![](https://gitlab.com/Antoniii/plot-van-gnuplot/-/raw/main/errorbar.png)

![](https://gitlab.com/Antoniii/plot-van-gnuplot/-/raw/main/errorline.png)

## Sources

* [Errorbars](http://www.gnuplot.info/docs_4.2/node140.html)
* [Errorlines ](http://www.gnuplot.info/docs_4.2/node141.html)
* [How to save a graph through command line with Gnuplot?](https://stackoverflow.com/questions/29625776/how-to-save-a-graph-through-command-line-with-gnuplot)
